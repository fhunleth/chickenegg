defmodule Mix.Tasks.Gen.CommandClasses do
  use Mix.Task

  @shortdoc "Generate the Command Class Module file"
  def run(_), do: :ok
end
