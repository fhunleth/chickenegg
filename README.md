# Chicken and egg problem

Here's how you reproduce it:

```sh
# Make sure that you have a clean repository
git clean -fdx

# First make
cd fw
export MIX_TARGET=bbb
mix deps.get
mix firmware

# See error

mix firmware

# See good build
```

